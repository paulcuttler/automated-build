﻿clear;
foreach ($filter in ("cmd")) {
    foreach ($process in (get-process $filter).Id)
    {
        $line = (Get-CimInstance Win32_Process -Filter "ProcessId = $process").CommandLine;
        
        if ($line -like "*jboss*")
        {
            # start the shutdown script by sending it a keypress. This will be absorbed by the "Press any key to continue..."
            # at the end of the shutdown script.
            $wshell = New-Object -ComObject wscript.shell;
            $wshell.AppActivate('Local jBoss Debug')

            echo 'k' | C:\devtools\jboss-4.2.2.GA\bin\shutdown.bat -S

            Sleep 30
            $wshell.SendKeys('~')
        }
    }
}

