-- Set to Y to drop the daltonc schema
DECLARE @cleanupDaltoncSchema CHAR(1) = 'Y';

-- Set to Y to drop tables that appear to be backups of main tables
DECLARE @cleanupBackupTables  CHAR(1) = 'Y';



DELETE	
FROM	[PUB].[cl-master]
WHERE	[client-no] <> 0	-- Leave the cash sales client in there, it might be important
AND		[client-no] NOT IN (817466,891187)	-- leave a couple of members for testing - Mine and Marnies :)


DELETE
FROM	[PUB].[mem_membership]
WHERE	client_number NOT IN 
(
	SELECT [client-no]
	FROM pub.[cl-master]
)


-- This takes forever - truncate would be better if we can
DELETE
FROM	[PUB].mem_document
WHERE	membership_id NOT IN
(
	SELECT	membership_id
	FROM	pub.mem_membership
)

DELETE
FROM	[PUB].mem_transaction
WHERE	membership_id NOT IN
(
	SELECT	membership_id
	FROM	pub.mem_membership
)

DELETE
FROM	pub.[cl-history]
WHERE	[client-no] NOT IN
(
	SELECT	[client-no]
	FROM	[PUB].[cl-master]
)


DELETE
FROM	[PUB].mem_membership_quote
WHERE	membership_id NOT IN
(
	SELECT	membership_id
	FROM	pub.mem_membership
)


DELETE
FROM	pub.pt_payableItem
WHERE	[clientno] NOT IN
(
	SELECT	[client-no]
	FROM	[PUB].[cl-master]
)


DELETE
FROM	PUB.pt_payableItemComponent
WHERE	payableItemID NOT IN
(
	SELECT	payableItemID
	FROM	pub.pt_payableItem
)


DELETE
FROM	PUB.pt_payableItemPosting
WHERE	payableItemComponentID NOT IN
(
	SELECT	payableItemComponentID
	FROM	PUB.pt_payableItemComponent
)


DELETE
FROM	pub.mem_transaction_fee_discount
WHERE	transaction_id NOT IN
(
	SELECT	transaction_id
	FROM	pub.mem_transaction
)


DELETE
FROM	pub.mem_transaction_fee
WHERE	transaction_id NOT IN
(
	SELECT	transaction_id
	FROM	pub.mem_transaction
)


DELETE
FROM	pub.mem_membership_card
WHERE	membership_id NOT IN
(
	SELECT	membership_id
	FROM	pub.mem_membership
)


DELETE
FROM	pub.pt_pending_fee
WHERE	[client_number] NOT IN
(
	SELECT	[client-no]
	FROM	[PUB].[cl-master]
)




IF @cleanupDaltoncSchema = 'Y' AND EXISTS (SELECT * FROM sys.schemas WHERE name = 'daltonc')
BEGIN
	PRINT 'Preparing to delete the daltonc schema...'


	DECLARE @MySchemaName VARCHAR(50)='daltonc', 
			@sql VARCHAR(MAX)='';

	DECLARE @SchemaName VARCHAR(255), 
			@ObjectName VARCHAR(255), 
			@ObjectType VARCHAR(255), 
			@ObjectDesc VARCHAR(255), 
			@Category INT;

	DECLARE cur CURSOR FOR
		SELECT  (s.name)SchemaName, (o.name)ObjectName, (o.type)ObjectType,(o.type_desc)ObjectDesc,(so.category)Category
		FROM    sys.objects o
		INNER JOIN sys.schemas s ON o.schema_id = s.schema_id
		INNER JOIN sysobjects so ON so.name=o.name
		WHERE s.name = @MySchemaName
		AND so.category=0
		AND o.type IN ('P','PC','U','V','FN','IF','TF','FS','FT','PK','TT')
		-- PK -- 	PRIMARY KEY constraint
		-- TT --	Table type
		ORDER BY 
			CASE 
				WHEN o.type IN ('FN', 'IF', 'TF', 'FS', 'FT') THEN 1
				WHEN o.type = 'V' THEN 2
				WHEN o.type =  'U' THEN 3
				WHEN o.type IN ('P','PC') THEN 4
			END

	OPEN cur

	FETCH NEXT FROM cur INTO @SchemaName,@ObjectName,@ObjectType,@ObjectDesc,@Category

	SET @sql='';

	-- script out drops for contraints - get them all done first
	SELECT @sql = @sql + N'ALTER TABLE ' + QUOTENAME(s.name) + N'.'  + QUOTENAME(t.name) + N' DROP CONSTRAINT ' + QUOTENAME(c.name) + ';' + CHAR(13)
	FROM	sys.objects AS c INNER JOIN 
			sys.tables AS t ON c.parent_object_id = t.[object_id] INNER JOIN
			sys.schemas AS s ON t.[schema_id] = s.[schema_id]
	WHERE	c.[type] IN ('D','C','F','PK','UQ')
	AND		s.name = @MySchemaName
	ORDER BY c.[type];


	-- loop through the cursor and generate drop statements for everything else
	WHILE @@FETCH_STATUS = 0 
	BEGIN    
		IF @ObjectType IN('FN', 'IF', 'TF', 'FS', 'FT') SET @sql=@sql+'Drop Function '+@MySchemaName+'.'+@ObjectName+CHAR(13)
		IF @ObjectType IN('V') SET @sql=@sql+'Drop View '+@MySchemaName+'.'+@ObjectName+CHAR(13)
		IF @ObjectType IN('P') SET @sql=@sql+'Drop Procedure '+@MySchemaName+'.'+@ObjectName+CHAR(13)
		IF @ObjectType IN('U') SET @sql=@sql+'Drop Table '+@MySchemaName+'.'+@ObjectName+CHAR(13)

		--PRINT @ObjectName + ' | ' + @ObjectType
		FETCH NEXT FROM cur INTO @SchemaName,@ObjectName,@ObjectType,@ObjectDesc,@Category
	END
	CLOSE cur;    
	DEALLOCATE cur;

	SET @sql=@sql+CASE WHEN LEN(@sql)>0 THEN 'Drop Schema '+@MySchemaName+CHAR(13) ELSE '' END

	--PRINT @sql

	EXECUTE (@sql)

END

-- drop any tables we things are backup copies of main tables
IF @cleanupBackupTables = 'Y'
BEGIN
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PUB].[cl-master_20131113]') AND type in (N'U'))
	BEGIN
		DROP TABLE [PUB].[cl-master_20131113]
	END

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cl-master_20140213]') AND type in (N'U'))
	BEGIN
		DROP TABLE [dbo].[cl-master_20140213]
	END
END



-- rebuild indexes to make sure we have released space
EXEC sp_msforeachtable 'SET QUOTED_IDENTIFIER ON; ALTER INDEX ALL ON ? REBUILD'

