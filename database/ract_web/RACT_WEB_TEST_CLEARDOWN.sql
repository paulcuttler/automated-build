-- Set to Y to drop tables that appear to be backups of main tables
DECLARE @cleanupBackupTables  CHAR(1) = 'Y';


-- Clean up the old insurance data
TRUNCATE TABLE WebInsQuoteDetail
DELETE FROM WebInsQuote


TRUNCATE TABLE WebInsClientDetail
DELETE FROM WebInsClient

DELETE 
FROM	WebClient
WHERE	clientType = 'insurance'


-- drop any tables we things are backup copies of main tables
IF @cleanupBackupTables = 'Y'
BEGIN
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users_20091215]') AND type in (N'U'))
	BEGIN
		DROP TABLE [dbo].[Users_20091215]
	END

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users_20110415]') AND type in (N'U'))
	BEGIN
		DROP TABLE [dbo].[Users_20110415]
	END

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WebInsQuoteDetail_20150930]') AND type in (N'U'))
	BEGIN
		DROP TABLE [dbo].[WebInsQuoteDetail_20150930]
	END
END


-- rebuild indexes to make sure we have released space
EXEC sp_msforeachtable 'SET QUOTED_IDENTIFIER ON; ALTER INDEX ALL ON ? REBUILD'

